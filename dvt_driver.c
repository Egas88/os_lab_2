#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/fs.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>

#include <linux/slab.h>
#include <linux/pid.h>
#include <linux/vmalloc.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/sched.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("Stab Linux module for lab");
MODULE_VERSION("1.0");

#define RW_TASK _IOWR('a', 'a', struct my_task_struct*)
#define RW_PAGE _IOWR('a', 'b', struct page_array*)
//#define RW_PID _IOWR('b', 'a', int*)

struct my_page_struct {
    unsigned long flags;
    unsigned long mapping;
    //int size;
    //char* read;
};

struct page_array {
    bool valid;
    pid_t pid;
	int size;
    struct my_page_struct** data;
};

struct my_task_struct {
    bool valid;
    unsigned long state;
    unsigned long policy;
    pid_t pid;
    pid_t tgid;
    int exit_state;
};


void task_method(struct my_task_struct* mts);
void page_method(struct page_array* pa, struct my_page_struct** data, unsigned long arg);

static int driver_open(struct inode* device_file, struct file* instance) {
    printk(KERN_INFO "open was called!\n");
    return 0;
}

static int driver_close(struct inode* device_file, struct file* instance) {
    printk(KERN_INFO "close was called!\n");
    return 0;
}

static struct page* get_current_page(struct mm_struct* mm, long virtual_address) {
    pgd_t* pgd;
    p4d_t* p4d;
    pud_t* pud;
    pmd_t* pmd;
    pte_t* pte;

    struct page* page = NULL;

    pgd = pgd_offset(mm, virtual_address);
    if (!pgd_present(*pgd)) {
        return NULL;
    }
    p4d = p4d_offset(pgd, virtual_address);
    if (!p4d_present(*p4d)) {
        return NULL;
    }
    pud = pud_offset(p4d, virtual_address);
    if (!pud_present(*pud)) {
        return NULL;
    }
    pmd = pmd_offset(pud, virtual_address);
    if (!pmd_present(*pmd)) {
        return NULL;
    }
    pte = pte_offset_kernel(pmd, virtual_address);
    if (!pte_present(*pte)) {
        return NULL;
    }
    page = pte_page(*pte);
    return page;
}

void task_method(struct my_task_struct* mts) {
    struct task_struct* ts = get_pid_task(find_get_pid(mts->pid), PIDTYPE_PID);

    if (ts == NULL) {
        mts->valid = false;
        return;
    }
    else {
        mts->valid = true;
        mts->state = ts->state;
        mts->policy = ts->policy;
        mts->tgid = ts->tgid;
        mts->exit_state = ts->exit_state;
    }
}

void page_method(struct page_array* pa, struct my_page_struct** data, unsigned long arg) {
    if (pa->pid == NULL) {
        printk(KERN_ERR "page pid == null\n");
    }
    struct task_struct* ts = get_pid_task(find_get_pid(pa->pid), PIDTYPE_PID);
    if (ts == NULL) {
        printk(KERN_ERR "ts == null\n");
        pa->valid = false;
        return;
    }
    else {
        struct page* page_struct;
        struct mm_struct* mm = ts->mm;
        if (mm == NULL) {
            pa->valid = false;
            printk(KERN_ERR "mm == NULL\n");
            return;
        }
        else {
            pa->valid = true;
            struct vm_area_struct* vas = mm->mmap;
            long virtual_address;
            int i = 0;
            while (vas) {
                for (virtual_address = vas->vm_start; virtual_address <= vas->vm_end; virtual_address += PAGE_SIZE) {
                    page_struct = get_current_page(mm, virtual_address);
                    if (page_struct != NULL && page_struct->mapping != 0 && i <= (pa->size)/sizeof(struct my_page_struct*)) {
                        data[i] = kmalloc(sizeof(struct my_page_struct), GFP_KERNEL);
                        data[i]->mapping = page_struct->mapping;
                        data[i]->flags = page_struct->flags;
                        if (copy_to_user((pa->data)[i], data[i], sizeof(struct my_page_struct))) {
                            printk(KERN_ERR "can't write to data");
                            return -EFAULT;
                        }
                        i++;
                    }
                }
                vas = vas->vm_next;
            }
        }
    }
}

static long int my_ioctl(struct file* file, unsigned cmd, unsigned long arg) {
    struct page_array pa;
    struct my_task_struct mts;
    switch (cmd) {
    case RW_TASK:
        if (copy_from_user(&mts, (struct my_task_struct*)arg, sizeof(mts))) {
            printk(KERN_ERR "can't read PID");
            return -EFAULT;
        }
        task_method(&mts);
        if (copy_to_user((struct my_task_struct*)arg, &mts, sizeof(mts))) {
            printk(KERN_ERR "can't write task");
            return -EFAULT;
        }
        break;
    case RW_PAGE:
        if (copy_from_user(&pa, (struct page_array*)arg, sizeof(pa))) {
            printk(KERN_ERR "can't read PID");
            return -EFAULT;
        }
        struct my_page_struct** data = kmalloc(pa.size, GFP_KERNEL);
        if (!data) {
            printk(KERN_ERR "can't allocate mem");
            return -EFAULT;
        }
        /*if ((READBUF*sizeof(struct my_page_struct*)) < pa.size) {
            printk(KERN_ERR "length is too big\n");
            return -ENOMEM;
        }*/
        page_method(&pa, data, arg);
        if (copy_to_user((struct page_array*)arg, &pa, sizeof(pa))) {
            printk(KERN_ERR "can't write page");
            return -EFAULT;
        }
        /*if (copy_to_user(pa.data, data, pa.size)) {
            printk(KERN_ERR "can't write string");
            return -EFAULT;
        }*/
        int i;
        for (i = 0; i < pa.size/sizeof(struct my_page_struct*); i ++) {
            if (copy_to_user((pa.data)[i], data[i], sizeof(struct my_page_struct))) {
                printk(KERN_ERR "can't write to data");
                return -EFAULT;
            }
        }
        for (i = 0; i < pa.size/sizeof(struct my_page_struct*); i ++) {
            kfree(data[i]);
        }
        kfree(data);
        break;
    default:
        printk(KERN_INFO "Такой команды нет");
        break;
    }
    return 0;
}

static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = driver_open,
    .release = driver_close,
    .unlocked_ioctl = my_ioctl
};

#define MYMAJOR 64

static int __init custom_init(void) {
    int retval;
    printk(KERN_INFO "Hello Kernel\n");
    retval = register_chrdev(MYMAJOR, "my_ioctl_exmple", &fops);
    if (retval == 0) {
        printk(KERN_INFO "registered device number Major: %d, Minor: %d\n", MYMAJOR, 0);
    }
    else {
        printk(KERN_INFO "could not register device number\n");
        return -1;
    }
    return 0;
}

static void __exit custom_exit(void) {
    unregister_chrdev(MYMAJOR, "my_ioctl_example");
    printk(KERN_INFO "kmod: module unloaded\n");
}

module_init(custom_init);
module_exit(custom_exit);
