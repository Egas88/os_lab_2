#include <sys/ioctl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdbool.h>

#define RW_TASK _IOWR('a', 'a', struct my_task_struct*)
#define RW_PAGE _IOWR('a', 'b', struct page_array*)
//#define RW_PID _IOWR('b', 'a', int*)
char* flags[] = {
    "PG_locked",
    "PG_referenced",
    "PG_uptodate",
    "PG_dirty",
    "PG_lru",
    "PG_active",
    "PG_workingset",
    "PG_waiters",
    "PG_error",
    "PG_slab",
    "PG_owner_priv_1",
    "PG_arch_1",
    "PG_reserved",
    "PG_private",
    "PG_private_2",
    "PG_writeback",
    "PG_head",
    "PG_mappedtodisk",
    "PG_reclaim",
    "PG_swapbacked",
    "PG_unevictable",
    "PG_mlocked",
    "PG_uncached",
    "PG_hwpoison",
    "PG_young",
    "PG_idle",
    "PG_arch2",
    "PG_skip_kasan_poison"
};

char * states[] = {
	"INTERRUPTIBLE",
	"UNINTERRUPTIBLE",
	"STOPPED",
	"TRACED",
	"DEAD",
	"WAKEKILL",
	"WAKING",
	"PARKED",
	"NOLOAD",
	"STATE_MAX"
};

struct my_page_struct {
    unsigned long flags;
    unsigned long mapping;
    //int size;
    //char* read;
};

struct page_array {
    bool valid;
    pid_t pid;
	int size;
    struct my_page_struct** data;
};

struct my_task_struct {
	bool valid;
    unsigned long state;
    unsigned long policy;
    pid_t pid;
    pid_t tgid;
    int exit_state;
};

void print_flags(unsigned long pg_flags) {
    unsigned long mask = 0x00000001;
    int i;
	printf("\tpage flags:\n");
    for (i = 0; i < 28; ++i) {
        if (pg_flags & mask) {
            printf("\t\t%s\n", flags[i]);
        }
        mask *= 2;
    }
}

void print_state(unsigned long state) {
	unsigned long mask = 1;
	if (state == 0) {
		printf("\tstate = RUNNING\n");
	}
	for (int i = 0; i < 4; ++i) {
		if (state == mask) {
			printf("\tstate=%s\n", states[i]);
		}
		mask*=2;
	}
	mask = 64;
	for (int i = 5; i < 10; ++i) {
		if (state == mask) {
			printf("\tstate=%s\n", states[i]);
		}
		mask*=2;
	}
}

void print_exit_state(int exit_state) {
	if (exit_state == 16) {
		printf("\tstate = DEAD\n");
	}
	if (exit_state == 32) {
		printf("\tstate = ZOMBIE\n");
	}
}

void free_pa(struct page_array* pa) {
	for (int i = 0; i < (pa->size) / sizeof(struct my_page_struct*); i++) {
		free((pa->data)[i]);
	}
	free(pa->data);
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("Некорректное число аргументов\n");
		return 0;
	}

	pid_t console_pid = atoi(argv[1]);
	int dev;

	struct page_array pa;
	struct my_task_struct mts;

	int n = 10000;

	pa.pid = console_pid;
	pa.size = n * sizeof(struct my_page_struct*);
	pa.data = (struct my_page_struct**) malloc(pa.size);
	for (int i = 0; i < pa.size/sizeof(struct my_page_struct*); i++) {
		(pa.data)[i] = malloc(sizeof(struct my_page_struct));
	}
	mts.pid = console_pid;

	dev = open("/dev/my_new_device", O_RDWR);
	if (dev == -1) {
		printf("\nОшибка при открытии\n");
		return -1;
	}
	printf("\nОткрытие прошло успешно\n");
	if (ioctl(dev, RW_PAGE, (struct page_array*) &pa)) {
		perror("error with ioctl: not enough memory");
	}
	if (pa.valid) {
		printf("Pages structs:\n");
		for (int i = 0; i < pa.size/sizeof(struct my_page_struct*); i++) {
			if (((pa.data)[i])->mapping != 0 && ((pa.data)[i])->flags != 0) {
				printf("\tmapping: %lx\n", ((pa.data)[i])->mapping);
				print_flags(((pa.data)[i])->flags);
			}
		}
		free_pa(&pa);
	} else {
		printf("nothing is fine!");
	}
	close(dev);

	dev = open("/dev/my_new_device", O_TRUNC | O_RDWR);
	if (dev == -1) {
		printf("\nОшибка при открытии\n");
		return -1;
	}
	ioctl(dev, RW_TASK, (struct my_task_struct*) &mts);
	if (mts.valid) {
		printf("\nTask struct TGID %d: \n", mts.tgid);
		print_state(mts.state);
		printf("\tpolicy = %ld\n", mts.policy);
		printf("\texit_state = %d\n", mts.exit_state);
	} else {
		printf("\nОшибка! Невозможно получить структуру task!");
	}
	close(dev);
	return 0;
}

