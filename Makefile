obj-m += dvt_driver.o
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
	sudo rmmod dvt_driver.ko
	sudo insmod dvt_driver.ko
